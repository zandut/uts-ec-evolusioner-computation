%===============================================================================
% Mendekodekan kromosom yang berisi bilangan Integer menjadi individu x yang
% bernilai real dalam interval yang ditentukan [Ra,Rb]
%
% Masukan 
%   Kromosom : kromosom, matriks berukuran 1 x JumGen
%   Nvar : jumlah variabel
%   Ng : jumlah gen yang mengkodekan satu variabel
%   Ra : batas atas interval
%   Rb : batas bawah interval
%   
% Keluaran 
%   x : individu hasil dekode kromosom
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function x = DekodeKromosomReal(Kromosom,Nvar,Ng,Ra,Rb)

for ii=1:Nvar,
  x(ii) = Rb + ((Ra - Rb) * Kromosom(ii));
end