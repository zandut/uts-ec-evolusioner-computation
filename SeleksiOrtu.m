
%===============================================================================
% Seleksi Ortu: memilih kromosom sebagai ortu yang akan masuk ke MatingPool  
%
% Masukan 
%   x : 2D-array berisi kromosom2 pada populasi
%   LF : Linear Fitness
%   
% Keluaran 
%   MatingPool : 2D-array berisi kromosom yang terpilih sebagai ortu dan
%   sudah dilakukan pengacakan pasangan
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function MatingPool = SeleksiOrtu(x, LF)

L = length(x(:,1));
ArrOrtuId = [];

% Pemilihan ortu berbasis Roulette Wheel
for ii=1:L,
    OrtuId = RouletteWheel(L,LF);
    ArrOrtuId = [ArrOrtuId OrtuId];
end

% Pengacakan pasangan
IndeksAcak = randperm(length(ArrOrtuId));
for jj=1:length(IndeksAcak),
    MatingPool(jj,:) = x(ArrOrtuId(IndeksAcak(jj)),:);
end