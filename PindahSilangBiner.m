%===============================================================================
% Memindah-silangkan bagian kromosom Ortu1 dan Ortu2 yang dipotong 
% secara random, sehingga dihasilkan dua buah kromosom Anak
%
% Masukan 
%   Ortu1 : kromosom, matriks berukuran 1 x JumGen
%   Ortu2 : kromosom, matriks berukuran 1 x JumGen
%   JumGen : jumlah gen
%   
% Keluaran 
%   Anak : kromosom hasil pindah silang, matriks berukuran 1 x JumGen
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function Anak = PindahSilangBiner(Ortu1,Ortu2,JumGen)

% Membangkitkan satu titik potong (TP bernilai antara 1 sampai JumGen-1)
TP = 1 + fix(rand*(JumGen-1));

% Anak 1 berisi bagian depan Ortu1 dan bagian belakang Ortu2
Anak(1,:) = [Ortu1(1:TP) Ortu2(TP+1:JumGen)];
Anak(2,:) = [Ortu2(1:TP) Ortu1(TP+1:JumGen)];