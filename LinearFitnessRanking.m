%===============================================================================
% Men-skala-kan nilai fitness ke dalam ranking sehingga diperoleh
% nilai-nilai fitness baru yang berada dalam rentang [MaxF,MinF]
%
% Masukan 
%   UkPop : ukuran populasi atau jumlah kromosom dalam populasi
%   Fitness: nilai fitness, matriks ukuran 1 x UkPop
%   MaxF : nilai fitness maximum
%   MinF : nilai fitness minimum
%   
% Keluaran 
%   LFR : Linear Fitness Ranking
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function LFR = LinearFitnessRanking(UkPop,Fitness,S)

% SF berisi nilai fitness yang terurut dari kecil ke besar (ascending)
% IndF berisi index dari nilai fitness yang menyatakan nomor urut kromosom
[SF,IndF] = sort(Fitness);

% LinearFitness = nilai fitness baru hasil pen-skala-an
for rr=1:UkPop,
   LFR(rr) = (2-S) + 2*(S-1) * ((IndF(rr)-1)/(UkPop-1));
end
