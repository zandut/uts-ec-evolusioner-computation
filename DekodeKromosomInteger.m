%===============================================================================
% Mendekodekan kromosom yang berisi bilangan Integer menjadi individu x yang
% bernilai real dalam interval yang ditentukan [Ra,Rb]
%
% Masukan 
%   Kromosom : kromosom, matriks berukuran 1 x JumGen
%   Nvar : jumlah variabel
%   Ng : jumlah bit yang mengkodekan satu variabel
%   Ra : batas atas interval
%   Rb : batas bawah interval
%   
% Keluaran 
%   x : individu hasil dekode kromosom
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function x = DekodeKromosomInteger(Kromosom,Nvar,Ng,Ra,Rb)

MaxSum = 0;
for kk=1:Ng,
  MaxSum = MaxSum + (9 * 10^(-kk));
end

for ii=1:Nvar,
  x(ii) = 0;
  for jj=1:Ng,
    x(ii) = x(ii) + Kromosom((ii-1)*Ng+jj)*10^(-jj);
  end
  x(ii) = Rb + ((Ra-Rb)/MaxSum) * x(ii);
end