%===============================================================================
% Algoritma Genetika Standar (dengan tampilan grafis 2D)
% Tujuan: mencari nilai minimum suatu fungsi
%
% Spesifikasi:
% 1. Satu populasi dengan jumlah kromosom = UkPop
% 2. Binary, Integer, atau Real encoding
% 3. Linear Fitness Ranking (opsional, bisa dipakai atau tidak)
% 4. Roulette-Wheel selection
% 5. Pindah Silang: Binary, Integer, atau Real dengan satu titik potong
% 6. Mutasi: Binary, Integer, atau Real
% 7. Probabilitas pindah silang dan probabilitas mutasi bernilai tetap
% 8. Generational replacement: mengganti semua individu dengan individu baru
% 9. Elitisme: satu atau dua kopi kromosom terbaik hidup di populasi berikutnya
%
% Updated 05 Oktober 2011
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

clc                           % Me-refresh command window
clear all                     % Menghapus semua semua variabel aktif

%===============================================================================
% Variabal yang bisa dilakukan pengaturan:
% 1. Representasi : 'BINER', 'INTEGER', atau 'REAL'
% 2. GenPerVar    : Jumlah gen yang mengkodekan satu variabel
% 3. UkPop        : Jumlah kromosom dalam populasi
% 4. Psilang      : Probabilitas rekombinasi atau cross-over
% 5. Pmutasi      : Probabilitas mutasi
% 6. MaxG         : Jumlah generasi maksimum
% 7. BilKecil     : Bilangan Kecil
% 8. PakaiLFR     : Boolean untuk kondisi pemakaian linear fitness ranking
% 9. S            : Selective pressure (pengatur linear fitness ranking)
%==============================================================================

Nvar         =  2;               % Jumlah variabel pada fungsi yang dioptimasi
Ra           =  3;            % Batas atas interval
Rb           = -3;            % Batas bawah interval

% Representasi = 'biner';          % Representasi individu ke dalam kromosom
% Representasi = 'integer';        % Representasi individu ke dalam kromosom
Representasi = 'real';           % Representasi individu ke dalam kromosom
GenPerVar    = 2;               % Jumlah gen yang mengkodekan satu variabel
TotGen       = GenPerVar*Nvar;   % Total gen dalam suatu kromosom (semua variabel)
UkPop        = 800;              % Jumlah kromosom dalam populasi
Psilang      = 0.9;              % Probabilitas rekombinasi atau cross-over
Pmutasi      = 2/GenPerVar;      % Probabilitas mutasi
MaxG         = 30;              % Jumlah generasi maksimum
BilKecil     = 1.04701;            % Digunakan untuk menghindari pembagian dengan 0
PakaiLFR     = 0;                % Boolean untuk kondisi pemakaian linear fitness ranking
S            = -1.03;              % Selective pressure (pengatur linear fitness ranking)

%===============================================================================

if strcmp(upper(Representasi), 'REAL'),
    GenPerVar = 1;
    TotGen = GenPerVar*Nvar;    % Total gen dalam suatu kromosom (semua variabel)
end

Fthreshold = (1/BilKecil) * 70;        % Threshold nilai Fitness untuk menghentikan evolusi
Bgraf      = Fthreshold;        % Untuk menangani tampilan grafis

% Inisialisasi grafis 2D
hfig = figure;
hold on
title('Optimasi fungsi menggunakan AG standar dengan grafis 2 dimensi')
set(hfig, 'position', [50,50,600,400]);
set(hfig, 'DoubleBuffer', 'on');
axis([1 MaxG 0 Bgraf]);
hbestplot = plot(1:MaxG,zeros(1,MaxG));
htext1 = text(0.5*MaxG,0.35*Bgraf,sprintf('Fitness terbaik: %15.11f', 0.0));
htext2 = text(0.5*MaxG,0.30*Bgraf,sprintf('Variabel X1: %12.11f', 0.0));
htext3 = text(0.5*MaxG,0.25*Bgraf,sprintf('Variabel X2: %12.11f', 0.0));
htext4 = text(0.5*MaxG,0.20*Bgraf,sprintf('Nilai minimum: %12.11f', 0.0));
htext5 = text(0.5*MaxG,0.15*Bgraf,sprintf('Ukuran populasi: %3.0f', 0.0));
htext6 = text(0.5*MaxG,0.10*Bgraf,sprintf('Prob. crossover: %12.11f', 0.0));
htext7 = text(0.5*MaxG,0.05*Bgraf,sprintf('Prob. mutasi: %12.11f', 0.0));
xlabel('Generasi');
ylabel('Fitness terbaik');
hold off
drawnow;

% Inisialisasi populasi
if strcmp(upper(Representasi), 'BINER'),
    Populasi = InisialisasiPopulasiBiner(UkPop,TotGen);
elseif strcmp(upper(Representasi), 'INTEGER'),
    Populasi = InisialisasiPopulasiInteger(UkPop,TotGen);
elseif strcmp(upper(Representasi), 'REAL'),
    Populasi = InisialisasiPopulasiReal(UkPop,TotGen);
end

disp('Inisialisasi populasi telah selesai');

% Loop evolusi
for generasi=1:MaxG,
    
    if strcmp(upper(Representasi), 'BINER'),
        x = DekodeKromosomBiner(Populasi(1,:),Nvar,GenPerVar,Ra,Rb);
    elseif strcmp(upper(Representasi), 'INTEGER'),
        x = DekodeKromosomInteger(Populasi(1,:),Nvar,GenPerVar,Ra,Rb);
    elseif strcmp(upper(Representasi), 'REAL'),
        x = DekodeKromosomReal(Populasi(1,:),Nvar,GenPerVar,Ra,Rb);
    end
    
    Fitness(1) = EvaluasiIndividu(x,BilKecil);
    MaxF = Fitness(1);
    MinF = Fitness(1);
    IndeksIndividuTerbaik = 1;
    for ii=2:UkPop,
        Kromosom = Populasi(ii,:);
        if strcmp(upper(Representasi), 'BINER'),
            x = DekodeKromosomBiner(Kromosom,Nvar,GenPerVar,Ra,Rb);
        elseif strcmp(upper(Representasi), 'INTEGER'),
            x = DekodeKromosomInteger(Kromosom,Nvar,GenPerVar,Ra,Rb);
        elseif strcmp(upper(Representasi), 'REAL'),
            x = DekodeKromosomReal(Kromosom,Nvar,GenPerVar,Ra,Rb);
        end
        Fitness(ii) = EvaluasiIndividu(x,BilKecil);
        if (Fitness(ii) > MaxF),
            MaxF = Fitness(ii);
            IndeksIndividuTerbaik = ii;
            BestX = x;
        end
        if (Fitness(ii) < MinF),
            MinF = Fitness(ii);
        end
    end
    
    % Penanganan grafis 2D
    plotvector = get(hbestplot,'YData');
    plotvector(generasi) = MaxF;
    set(hbestplot,'YData',plotvector);
    set(htext1,'String',sprintf('Fitness terbaik: %15.11f', MaxF));
    set(htext2,'String',sprintf('Variabel X1: %12.11f', BestX(1)));
    set(htext3,'String',sprintf('Variabel X2: %12.11f', BestX(2)));
    set(htext4,'String',sprintf('Nilai minimum: %12.11f', HasilPerhtungan(BestX)));
    set(htext5,'String',sprintf('Ukuran populasi: %3.0f', UkPop));
    set(htext6,'String',sprintf('Prob. crossover: %12.11f', Psilang));
    set(htext7,'String',sprintf('Prob. mutasi: %12.11f', Pmutasi));
    drawnow
    
    if MaxF >= Fthreshold,
        
        break;
        
    end
    
    % Jika menggunakan Linear Fitness Ranking
    if PakaiLFR,
        Fitness = LinearFitnessRanking(UkPop,Fitness,S);
    end
    
    TempPopulasi = Populasi;
    
    % Elitisme:
    % - Buat satu kopi kromosom terbaik jika ukuran populasi ganjil
    % - Buat dua kopi kromosom terbaik jika ukuran populasi genap
    if mod(UkPop,2)==0,           % ukuran populasi genap
        TempPopulasi(1,:) = Populasi(IndeksIndividuTerbaik,:);
        TempPopulasi(2,:) = Populasi(IndeksIndividuTerbaik,:);
        IterasiMulai = 3;
    else                          % ukuran populasi ganjil
        TempPopulasi(1,:) = Populasi(IndeksIndividuTerbaik,:);
        IterasiMulai = 2;
    end
    
    % Seleksi Orangtua
    MatingPool = SeleksiOrtu(TempPopulasi(IterasiMulai:UkPop, :), Fitness(IterasiMulai:UkPop));
    
    % Random Pasangan Orangtua
    MatingPool = MatingPool(randperm(length(MatingPool(:,1))),:);

    % Rekombinasi atau Pindah Silang: Satu titik potong
    for jj=1:2:length(MatingPool(:,1)),
        if (rand < Psilang),
            if strcmp(upper(Representasi), 'BINER'),
                Anak = PindahSilangBiner(MatingPool(jj,:),MatingPool(jj+1,:),TotGen);
            elseif strcmp(upper(Representasi), 'INTEGER'),
                Anak = PindahSilangInteger(MatingPool(jj,:),MatingPool(jj+1,:),TotGen);
            elseif strcmp(upper(Representasi), 'REAL'),
                Anak = PindahSilangReal(MatingPool(jj,:),MatingPool(jj+1,:),TotGen);
            end
            TempPopulasi(IterasiMulai+jj,:)   = Anak(1,:);
            TempPopulasi(IterasiMulai+jj+1,:) = Anak(2,:);
        else
            TempPopulasi(IterasiMulai+jj,:)   = MatingPool(jj,:);
            TempPopulasi(IterasiMulai+jj+1,:) = MatingPool(jj+1,:);
        end
    end
    
    % Mutasi dilakukan untuk setiap kromosom pada TempPopulasi
    for kk=IterasiMulai:UkPop,
        
        if strcmp(upper(Representasi), 'BINER'),
            TempPopulasi(kk,:) = MutasiBiner(TempPopulasi(kk,:),TotGen,Pmutasi);
        elseif strcmp(upper(Representasi), 'INTEGER'),
            TempPopulasi(kk,:) = MutasiInteger(TempPopulasi(kk,:),TotGen,Pmutasi);
        elseif strcmp(upper(Representasi), 'REAL'),
            TempPopulasi(kk,:) = MutasiReal(TempPopulasi(kk,:),TotGen,Pmutasi);
        end
    end
    % Generational Replacement: mengganti semua kromosom sekaligus
    Populasi = TempPopulasi;
end