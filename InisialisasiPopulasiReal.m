%===============================================================================
% Membangkitkan kromosom sejumlah UkPop, masing-masing kromosom berisi bilangan 
% biner (0 dan 1) sejumlah JumGen
%
% Masukan 
%   UkPop : ukuran populasi atau jumlah kromosom dalam populasi
%   JumGen: jumlah gen dalam suatu kromosom
%   
% Keluaran 
%   Populasi : kumpulan kromosom, matriks berukuran UkPop x JumGen
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function Populasi = InisialisasiPopulasiReal(UkPop,JumGen)

Populasi = rand(UkPop,JumGen);
