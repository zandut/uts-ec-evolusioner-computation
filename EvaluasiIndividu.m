%===============================================================================
% Mengevaluasi individu sehingga didapatkan nilai fitness-nya 
%
% Masukan 
%   x : individu
%   BilKecil : bilangan kecil digunakan untuk menghindari pembagian dengan 0
%   
% Keluaran 
%   fitness : nilai fitness
%
% Updated 18 Oktober 2010
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%==========================================================================

function fitness = EvaluasiIndividu(x,BilKecil)

fitness = (1 / (HasilPerhtungan(x) + BilKecil));