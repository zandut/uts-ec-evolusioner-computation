%===============================================================================
% Memindah-silangkan bagian kromosom Ortu1 dan Ortu2 menggunakan metode 
% Whole arithmetic crossover
%
% Masukan 
%   Ortu1 : kromosom, matriks berukuran 1 x JumGen
%   Ortu2 : kromosom, matriks berukuran 1 x JumGen
%   JumGen : jumlah gen
%   
% Keluaran 
%   Anak : kromosom hasil pindah silang, matriks berukuran 1 x JumGen
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function Anak = PindahSilangReal(Ortu1,Ortu2,JumGen)

Alfa = 0.8;  % Alfa bisa diset sesuai kebutuhan

% Whole arithmetic crossover
Anak(1,:) = (Alfa .* Ortu1) + (1 - Alfa) .* Ortu2;
Anak(2,:) = (Alfa .* Ortu2) + (1 - Alfa) .* Ortu1;