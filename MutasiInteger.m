%===============================================================================
% Mutasi gen Integer dengan probabilitas sebesar Pmutasi
% Gen-gen yang terpilih diubah nilainya menggunakan creep mutation
%
% Masukan
%   Kromosom : kromosom, matriks berukuran 1 x JumGen
%   JumGen : jumlah gen
%   Pmutasi : Probabilitas mutasi
%
% Keluaran
%   MutKrom : kromosom hasil mutasi, matriks berukuran 1 x JumGen
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function MutKrom = MutasiInteger(Kromosom,JumGen,Pmutasi)

MutKrom = Kromosom;
for ii=1:JumGen,
    if (rand < Pmutasi),
        a = fix(randn * 2);
        MutKrom(ii) = abs(a + Kromosom(ii));
        if MutKrom(ii) > 9,
            MutKrom(ii) = 9;
        end
    end
end