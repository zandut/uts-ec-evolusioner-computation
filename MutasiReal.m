%===============================================================================
% Mutasi gen Real dengan probabilitas sebesar Pmutasi
% Gen-gen yang terpilih diubah nilainya menggunakan creep mutation
%
% Masukan
%   Kromosom : kromosom, matriks berukuran 1 x JumGen
%   JumGen : jumlah gen
%   Pmutasi : Probabilitas mutasi
%
% Keluaran
%   MutKrom : kromosom hasil mutasi, matriks berukuran 1 x JumGen
%
% Updated 17 September 2009
% Suyanto - Fakultas Informatika - Institut Teknologi Telkom
% Jl Telekomunikasi No 1 Terusan Buah Batu - Bandung 40257
% www.ittelkom.ac.id/staf/suy
%===============================================================================

function MutKrom = MutasiReal(Kromosom,JumGen,Pmutasi)

CreepSize = 1/1000;
MutKrom = Kromosom;
for ii=1:JumGen,
    if (rand < Pmutasi),
        a = (randn * CreepSize);
        MutKrom(ii) = a + Kromosom(ii);
        if MutKrom(ii) > 1,
            MutKrom(ii) = 1;
        elseif MutKrom(ii) < 0,
            MutKrom(ii) = 0;
        end
    end
end